## Welcome!

This is the official *Meme Firm Without A Cool Acronym* website!

### Here Are Some Firm Rules:\
1. You must be an active investor to participate.\
2. Be nice.\
3. If for whatever reason you are unable to invest, please let me know.\
4. If you are inactive for more than 2 paydays without notice, I will notify you.\
   **If no response is received by the 3rd payday, you will be fired.**\
5. If you are new and do not invest at all \(and consistently\) after joining, we reserve the right to fire you.
