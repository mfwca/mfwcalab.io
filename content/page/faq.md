---
title: Questions that nobody asked but I'll answer anyways
comments: false
---

### Why is this hosted on gitlab?\
I don't make real money from this firm, so I won't put real money into it.

### How Do I Sign Up?\
Really? It's its own page. Right up top. No, not there, just click the button that says Apply Now, and you'll get whisked away to a google forms.

### What is the tax rate?\
40%

### What!?! That's so high!!!\
First off, not a question, but I usually invest more than anyone else in the firm, so the only person the taxes truly effect is me. This just means a bigger payout at the end of the week.